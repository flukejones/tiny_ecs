# Changelog

## [0.19.5] - 2020-07-11

- Switch to [Persist-O-Vec](https://crates.io/crates/persist-o-vec) for internal
  entity storage.
- Update docs.

## [0.19.4] - 2019-10-05

- Version bump for many small misc changes in doc comments and code.
- Update dependencies.

## [0.19.2] - 2019-10-05

- Add a changelog so that you can be informed.
- Remove custom rustfmt config.
- Add function `rm_entity` to effectively remove and entity *and* all of its associated components.

Other notable changes since this project started are:
- Feature to choose size of bit-mask used to track an entities components.
- Add `unsafe` versions of `borrow` and `borrow_mut` for component vec_map  which bypass the runtime borrow check.
  - `borrow_unchecked`
  - `borrow_mut_unchecked`
- Implement benchmarks.
