/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//! # Tiny ECS
//!
//! The intention of this crate is that a basic ECS is provided, where
//! you will be required to exercise a little additional control.
//!
//! Where most other ECS crates provide a mechanism for inserting "systems"
//! in to the ECS to run against entities, this one leaves it out - you can
//! think of it as a "system for entity/components". You will need to create
//! external systems; these can be a function, a loop, or anything else.
//!
//! Internally this ECS is the use of `bitmasks`. Each entity ID is in
//! practice an internal index number in to an array which contains bitmasks.
//! The bitmasks themselves keep track of what components the entity has.
//!
//! **Note:** borrows of `Persist` are checked at runtime, but there are also
//! some *unchecked* borrows provided.
//!
//! # Examples
//!
//! ## Demonstrating use
//!
//! ```
//! use tiny_ecs::Entities;
//! use tiny_ecs::ECSError;
//!
//! // These are the "components" we will use
//! struct Vector1 {
//!     x: i32,
//! }
//! struct Vector2 {
//!     x: i32,
//!     y: i32,
//! }
//! struct Vector3 {
//!     x: i32,
//!     y: i32,
//!     z: i32,
//! }
//! # fn main() -> Result<(), ECSError> {
//! // Initialize the Entity collection
//! let mut entities = Entities::new(Some(3), Some(3));
//!
//! // Creating an entity uses the builder pattern.
//! // Be sure to return the ID if you need to track it
//! let entity_1 = entities.new_entity()
//!                        .with(Vector1 { x: 42 })?
//!                        .with(Vector3 { x: 3, y: 10, z: -12 })?
//!                        .finalise()?;
//! // The entity ID will increment up from 0
//! assert_eq!(entity_1, 0);
//!
//! // Do note however, that you can keep adding parts to this entity
//! // via the builder pattern until you choose to create another entity
//!
//! // To add another entity you need to create one
//! entities.new_entity()
//!         .with(Vector2 { x: 66, y: 6 })?
//!         .with(Vector1 { x: 6 })?
//!         .finalise()?;
//! # Ok(())
//! # }
//!```
//!
//! ## Access an entities part of type `<T>`
//! ```
//! # use tiny_ecs::Entities;
//! # use tiny_ecs::ECSError;
//! # fn main() -> Result<(), ECSError> {
//! struct Vector3 {x: i32, y: i32, z: i32 }
//! # let mut entities = Entities::new(Some(3), Some(3));
//! # let entity_1 = entities.new_entity()
//! #                         .with(Vector3 { x: 3, y: 10, z: -12 })?
//! #                         .finalise()?;
//! // To get access to a part belonging to an entity you need
//! // first to get the component map created for the part type
//! // You need to 'anchor' this with a let or the ref is
//! // dropped before you can use it
//! let mut components = entities
//!     .borrow_mut::<Vector3>()?;
//! // You can then use the part by getting a reference
//! let mut part = components.get_mut(entity_1).unwrap();
//! assert_eq!(part.z, -12);
//! # Ok(())
//! # }
//! ```
//!
//! ## Check if `Entity` contains a part type + remove part
//! ```
//! # use tiny_ecs::Entities;
//! # use tiny_ecs::ECSError;
//! # fn main() -> Result<(), ECSError> {
//! struct Vector1 {x: i32 }
//! # let mut entities = Entities::new(Some(3), Some(3));
//! let entity_1 = entities.new_entity()
//!                        .with(Vector1 { x: 3 })?
//!                        .finalise()?;
//! // You can check if an entity contains a part with the type signature
//! if entities.entity_contains::<Vector1>(entity_1) {
//!     assert!(entities.rm_component_from::<Vector1>(entity_1).is_ok());
//! }
//! assert_eq!(entities.entity_contains::<Vector1>(entity_1), false);
//! # Ok(())
//! # }
//! ```
//!
//! ## A system that uses an `get_mut()`
//! ```
//! # use tiny_ecs::{Entities, Persist};
//! # use tiny_ecs::ECSError;
//! # fn main() -> Result<(), ECSError> {
//! struct Vector1 {x: i32 }
//! # let mut entities = Entities::new(Some(3), Some(3));
//! # entities.new_entity().with(Vector1 { x: 3 })?.finalise()?;
//!
//! // Make a system of some form that takes a `Persist<T>` arg
//! fn some_system(mut components: &mut Persist<Vector1>) {
//!     // You can then iterate over the components directly
//!     for (k, v) in components.iter_mut().enumerate() {
//!         v.x += 1;
//!         assert!(v.x > k as i32);
//!     }
//! }
//! # let mut components = entities.borrow_mut::<Vector1>()?;
//! some_system(&mut components);
//! # Ok(())
//! # }
//! ```
//!
//! ## Get components for an entity ID list
//! ```
//! # use tiny_ecs::{Entities, Persist};
//! # use tiny_ecs::ECSError;
//! # fn main() -> Result<(), ECSError> {
//! struct Vector1 {x: i32 }
//! # let mut entities = Entities::new(Some(3), Some(3));
//! # entities.new_entity().with(Vector1 { x: 3 })?.finalise()?;
//!
//! // A system that fetches the components for only the entities you are require
//! fn second_system(active: &[usize], mut v1_map: &mut Persist<Vector1>) {
//!     for id in active {
//!         if let Some(part) = v1_map.get_mut(*id) {
//!             part.x = 42;
//!         }
//!     }
//! }
//! # let mut components = entities.borrow_mut::<Vector1>()?;
//! second_system(&[0, 1, 2], &mut components);
//! # Ok(())
//! # }
//! ```
//!
//! ## A more complex system using Persists directly
//! ```
//! # use tiny_ecs::Entities;
//! # use tiny_ecs::ECSError;
//! # fn main() -> Result<(), ECSError> {
//! # struct Vector1 {x: i32 }
//! # struct Vector2 {x: i32, y: i32 }
//! # let mut entities = Entities::new(Some(3), Some(3));
//! # entities.new_entity()
//! # .with(Vector1 { x: 3 })?
//! # .with(Vector2 { x: 3, y: 3 })?
//! # .finalise()?;
//!
//! // Or a system handles the `Entities` container directly
//! fn other_system(active_ents: &[usize], entities: &mut Entities) -> Result<(), ECSError> {
//!     // You can mutably borrow multiple component maps at once
//!     let mut v1_components = entities
//!         .borrow_mut::<Vector1>()?;
//!
//!     let mut v2_components = entities
//!         .borrow_mut::<Vector2>()?;
//!
//!     // But not have a mutable borrow and immutable borrow to the same map
//!     // Fails at runtime!
//!     // let v2_components = entities.borrow::<Vector2>().unwrap();
//!     for id in active_ents {
//!         if entities.entity_contains::<Vector1>(*id) &&
//!            entities.entity_contains::<Vector2>(*id) {
//!             let v1_part = v1_components.get_mut(*id).unwrap();
//!             let v2_part = v2_components.get_mut(*id).unwrap();
//!             v1_part.x = 42;
//!             assert_ne!(v1_part.x, 43);
//!             assert_eq!(v1_part.x, 42);
//!         }
//!     }
//!     Ok(())
//! }
//! other_system(&[0, 1, 2], &mut entities);
//! # Ok(())
//! # }
//! ```

mod errors;

pub use crate::errors::*;
use hashbrown::HashMap;
pub use persist_o_vec::Persist;
use std::any::{Any, TypeId};
use std::cell::{Ref, RefCell, RefMut};
use std::ops::{Deref, DerefMut};

#[cfg(feature = "component_max_31")]
type BitMaskType = u32;
#[cfg(feature = "component_max_63")]
type BitMaskType = u64;
#[cfg(feature = "component_max_127")]
type BitMaskType = u128;

#[cfg(feature = "component_max_31")]
const BIT_MASK_MAX: BitMaskType = 31;
#[cfg(feature = "component_max_63")]
const BIT_MASK_MAX: BitMaskType = 63;
#[cfg(feature = "component_max_127")]
const BIT_MASK_MAX: BitMaskType = 127;

// Bitmask used to fill the initial mask list, and replace deleted entities
const EMPTY: BitMaskType = 0;

/// Immutable reference container for `Persist` returned by `Entities::borrow()`
///
/// This struct is required to contain a hidden borrow to the requested `Persist`.
/// The reason for this is so we can borrow multiple `Persist`, but not break the
/// borrow checker rules for borrows on a single `Persist`.
pub struct MapRef<'a, T> {
    _borrow: Ref<'a, dyn Any>,
    value: &'a Persist<T>,
}

impl<'a, T: 'static> MapRef<'a, T> {
    fn new(value: &'a RefCell<Box<dyn Any>>) -> Result<MapRef<'a, T>, ECSError> {
        let borrow = value.try_borrow().or(Err(ECSError::Borrow))?;

        // This is required to sidestep the borrow issue in root Entities struct
        let v = (unsafe { value.as_ptr().as_ref() })
            .ok_or(ECSError::PtrRef)?
            .downcast_ref::<Persist<T>>()
            .ok_or(ECSError::Downcast)?;

        // And make it safe by keeping the lifetime of the borrow with the downcast
        Ok(MapRef {
            value: v,
            _borrow: borrow,
        })
    }
}

impl<'a, T> Deref for MapRef<'a, T> {
    type Target = Persist<T>;

    #[inline]
    fn deref(&self) -> &Persist<T> {
        self.value
    }
}

/// Mutable reference container for `Persist` returned by `Entities::borrow_mut()`
///
/// This struct is required to contain a hidden borrow to the requested `Persist`.
/// The reason for this is so we can borrow multiple `Persist`, but not break the
/// borrow checker rules for borrows on a single `Persist`.
pub struct MapRefMut<'a, T> {
    _borrow: RefMut<'a, dyn Any>,
    value: &'a mut Persist<T>,
}

impl<'a, T: 'static> MapRefMut<'a, T> {
    #[inline]
    fn new(value: &'a RefCell<Box<dyn Any>>) -> Result<MapRefMut<'a, T>, ECSError> {
        let borrow = value.try_borrow_mut().or(Err(ECSError::BorrowMut))?;

        let v = (unsafe { value.as_ptr().as_mut() })
            .ok_or(ECSError::PtrMut)?
            .downcast_mut::<Persist<T>>()
            .ok_or(ECSError::DowncastMut)?;

        Ok(MapRefMut {
            value: v,
            _borrow: borrow,
        })
    }
}

impl<'a, T> Deref for MapRefMut<'a, T> {
    type Target = Persist<T>;

    #[inline]
    fn deref(&self) -> &Persist<T> {
        self.value
    }
}

impl<'a, T> DerefMut for MapRefMut<'a, T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Persist<T> {
        self.value
    }
}

/// This is the root of the ECS implementation
#[derive(Debug)]
pub struct Entities {
    // the index in to this Vec is the ID of the entity
    entity_masks: Vec<BitMaskType>,
    components: Vec<RefCell<Box<dyn Any>>>,
    next_free_entity: usize,
    current_entity: Option<usize>,
    vacated_slots: Vec<usize>,
    // BitMaskType is the mask assigned to the TypeId and is used to
    // quickly determine if an entity has a component type
    type_masks: HashMap<TypeId, (BitMaskType, usize)>,
    next_typemask: BitMaskType,
}

impl Default for Entities {
    /// Create a new `Entities` struct with no pre-allocated memory for maps
    fn default() -> Self {
        Entities::new(None, None)
    }
}

impl Entities {
    /// Create a new root entity container.
    ///
    /// - `entity_count` will initialize the entity map to this size. Good to
    ///   do if you know the max count of entities you will handle
    /// - `component_count` is as above, for total unique component types/kinds
    ///
    /// For `component_count` there is a maximum of either 32, 64, or 128 individual
    /// parts you can add of which the index starts at 0 to n-1, depending on
    /// which is enabled by the crate features
    pub fn new(entity_count: Option<usize>, component_count: Option<usize>) -> Entities {
        if let Some(count) = component_count {
            if count as BitMaskType > BIT_MASK_MAX {
                // Must panic if the component count is larger than the mask size
                // as we can only insert this many anyway
                panic!(
                    "Initial part count too large. Maximum of {} allowed",
                    BIT_MASK_MAX
                );
            }
        }
        Entities {
            entity_masks: vec![EMPTY; entity_count.unwrap_or(0)],
            components: Vec::with_capacity(component_count.unwrap_or(0)),
            next_free_entity: 0,
            current_entity: None,
            vacated_slots: Vec::new(),
            type_masks: HashMap::with_capacity(component_count.unwrap_or(0)),
            next_typemask: 1,
        }
    }

    /// Start the creation of a new entity
    ///
    /// If no parts are added to this call with `with()` then the entity will
    /// not be created.
    #[inline]
    pub fn new_entity(&mut self) -> &mut Self {
        if let Some(slot) = self.vacated_slots.pop() {
            self.current_entity = Some(slot);
        } else {
            self.current_entity = Some(self.next_free_entity);
            self.next_free_entity += 1;
        }
        self
    }

    /// Chained with `new_entity()` to add components
    ///
    /// `with()` can be chained multiple times to add many components.
    ///
    /// # Example
    ///
    /// ```
    /// # use tiny_ecs::Entities;
    /// # use tiny_ecs::ECSError;
    /// # fn main() -> Result<(), ECSError> {
    /// struct Component1 {}
    /// struct Component2 {}
    ///
    /// # let mut entities = Entities::new(Some(3), Some(3));
    /// let entity_1 = entities
    ///                 .new_entity()
    ///                 .with(Component1 {})?
    ///                 .with(Component2 {})?
    ///                 .finalise()?;
    /// assert_eq!(entity_1, 0);
    /// # Ok(())
    /// # }
    /// ```
    #[inline]
    pub fn with<T: 'static>(&mut self, part: T) -> Result<&mut Self, ECSError> {
        // Can't use .with() after entity is finalised
        if self.current_entity.is_none() {
            return Err(ECSError::WithAfterFinalise);
        }
        let type_id = TypeId::of::<T>();

        // if the component doesn't already have a type_mask entry
        let (type_mask, type_index) = {
            if !self.type_masks.contains_key(&type_id) {
                if (self.type_masks.len() as BitMaskType) >= BIT_MASK_MAX {
                    return Err(ECSError::BitMasksExhausted);
                }
                self.components
                    .push(RefCell::new(Box::new(Persist::<T>::with_capacity(
                        self.entity_masks.len() + self.vacated_slots.len(),
                    ))));
                self.type_masks
                    .insert(type_id, (self.next_typemask, self.components.len() - 1));
                self.next_typemask <<= 1;
            }
            self.type_masks[&type_id]
        };

        // TODO: overwrite? Crate user will need to be made aware when it happens if so
        // this shouldn't ever fail
        let entity_mask = self.entity_masks[self.current_entity.unwrap()];
        if entity_mask & type_mask == type_mask {
            return Err(ECSError::AddDupeComponent);
        }

        // This really shouldn't ever fail
        let id = self.current_entity.unwrap();
        if let Some(col) = self.components.get_mut(type_index) {
            col.borrow_mut()
                .downcast_mut::<Persist<T>>()
                .ok_or(ECSError::DowncastMut)?
                .insert(id, part);
            // update the entity mask
            let old = self.entity_masks[id];
            self.entity_masks[id] = old | type_mask;
        }
        Ok(self)
    }

    /// Optional final call in creating an entity - returns ID. You can begin
    /// using the entity without calling this, but it is recommended that you do
    /// to prevent workplace accidents.
    ///
    /// If you don't finalise an entity you can keep adding
    /// components to it later. This isn't recommended though
    /// as it's easy to lose track of which entity you are working
    /// on. The `add_component()` method allows you to add extra
    /// components to an existing entity if you know the ID.
    #[inline]
    pub fn finalise(&mut self) -> Result<usize, ECSError> {
        if let Some(entity) = self.current_entity {
            self.current_entity = None;
            return Ok(entity);
        }
        Err(ECSError::FinaliseNonEntity)
    }

    /// Check if an entity ID is valid (alive and has components)
    ///
    /// If `false` then there are no components attached to this ID and
    /// the entity is `None`.
    #[inline]
    pub fn entity_exists(&self, id: usize) -> bool {
        if let Some(ent) = self.entity_masks.get(id) {
            return *ent > 0;
        }
        false
    }

    #[inline]
    pub fn entity_contains<T: 'static>(&self, id: usize) -> bool {
        if let Some(entity_mask) = self.entity_masks.get(id) {
            if let Some((type_mask, _)) = self.type_masks.get(&TypeId::of::<T>()) {
                if entity_mask & type_mask == *type_mask {
                    return true;
                }
            }
        }
        false
    }

    #[deprecated]
    pub fn rm_component<T: 'static>(&mut self, id: usize) -> Result<(), ECSError> {
        self.rm_component_from::<T>(id)
    }
    /// Remove an entities part. If no components are left after part removal then
    /// the entity is considered deleted
    ///
    /// Removal requires the ID of the entity and the components type signature.
    ///
    /// # Example
    ///
    /// ```
    /// # use tiny_ecs::Entities;
    /// # use tiny_ecs::ECSError;
    /// # fn main() -> Result<(), ECSError> {
    /// #[derive(Debug, PartialEq)]
    /// struct Test1 {}
    ///
    /// # let mut entities = Entities::new(Some(3), Some(3));
    /// # let entity_1 = entities.new_entity()
    /// # .with(Test1 {})?.finalise()?;
    /// assert!(entities.rm_component_from::<Test1>(entity_1).is_ok());
    /// assert!(!entities.entity_contains::<Test1>(entity_1));
    /// # Ok(())
    /// # }
    /// ```
    #[inline]
    pub fn rm_component_from<T: 'static>(&mut self, id: usize) -> Result<(), ECSError> {
        let type_id = TypeId::of::<T>();
        if let Some((type_mask, type_index)) = self.type_masks.get(&type_id) {
            if let Some(map) = self.components.get_mut(*type_index) {
                map.borrow_mut()
                    .downcast_mut::<Persist<T>>()
                    .ok_or(ECSError::DowncastMut)?
                    .remove(id);
                self.entity_masks[id] ^= *type_mask;
                if self.entity_masks[id] == EMPTY {
                    self.vacated_slots.push(id)
                }
                return Ok(());
            }
        }
        Err(ECSError::NoComponentMap)
    }

    #[deprecated]
    pub fn add_component<T: 'static>(&mut self, id: usize, component: T) -> Result<(), ECSError> {
        self.add_component_to(id, component)
    }
    /// Add a component to the existing Entity ID
    ///
    /// # Example
    ///
    /// ```
    /// # use tiny_ecs::Entities;
    /// # use tiny_ecs::ECSError;
    /// # fn main() -> Result<(), ECSError> {
    /// struct Test1 {}
    /// struct Test2 {}
    /// struct Test3 {}
    ///
    /// let mut entities = Entities::new(Some(3), Some(3));
    /// let entity_1 = entities
    ///     .new_entity()
    ///     .with(Test1 {})?
    ///     .with(Test2 {})?
    ///     .finalise()?;
    /// entities.add_component_to(entity_1, Test3 {});
    /// assert!(entities.entity_contains::<Test1>(entity_1));
    /// # Ok(())
    /// # }
    /// ```
    #[inline]
    pub fn add_component_to<T: 'static>(
        &mut self,
        id: usize,
        component: T,
    ) -> Result<(), ECSError> {
        if let Some(ent) = self.entity_masks.get(id) {
            if *ent == 0 {
                return Err(ECSError::WithAfterFinalise);
            }
        }
        self.current_entity = Some(id);
        self.with(component)?.finalise()?;
        Ok(())
    }

    /// Get a plain reference to the selected entity part map. Borrow rules are checked at runtime.
    ///
    /// You may have multiple immutable references to the requested `Persist`
    /// **type** but no mutable references if the same **typed** `Persist`
    /// is currently referenced.
    ///
    /// - `Option` is whether or not there is a part of `<T>` for that entity.
    /// - Borrowing (`Ref`) is checked at runtime.
    ///
    /// # Example
    /// ```
    /// # use tiny_ecs::Entities;
    /// # use tiny_ecs::ECSError;
    /// # fn main() -> Result<(), ECSError> {
    /// # struct Test1 { x: u32 }
    /// # let mut entities = Entities::new(Some(3), Some(3));
    /// # let entity_1 = entities.new_entity()
    /// # .with(Test1 { x: 666 })?.finalise()?;
    /// let components = entities
    ///     .borrow::<Test1>()
    ///     .unwrap();
    /// let part = components.get(entity_1).unwrap();
    /// # Ok(())
    /// # }
    /// ```
    #[inline(always)]
    pub fn borrow<T: 'static>(&self) -> Result<MapRef<T>, ECSError> {
        let type_id = TypeId::of::<T>();
        if let Some((_, type_index)) = self.type_masks.get(&type_id) {
            if let Some(components) = self.components.get(*type_index) {
                return Ok(MapRef::new(components)?);
            }
        }
        Err(ECSError::NoComponentMap)
    }

    /// Allows you to borrow without runtime check overhead or to borrow when the standard
    /// borrow rules prevent you from doing so and you enforce safety
    ///
    /// # Safety
    /// Borrows are not checked at compile-time or runtime. An unchecked borrow
    /// on a mutably borrowed `Persist` is UB.
    #[inline(always)]
    pub unsafe fn borrow_unchecked<T: 'static>(&self) -> Result<&Persist<T>, ECSError> {
        let type_id = TypeId::of::<T>();
        if let Some((_, type_index)) = self.type_masks.get(&type_id) {
            if let Some(components) = self.components.get(*type_index) {
                let components = (&*components.as_ptr())
                    .downcast_ref::<Persist<T>>()
                    .ok_or(ECSError::Downcast)?;
                return Ok(components);
            }
        }
        Err(ECSError::NoComponentMap)
    }

    /// Get a mutable reference to the selected entity part map. Borrow rules are checked at runtime.
    ///
    /// You may have only one mutable reference to the requested `Persist`
    /// **type** and no immutable references. You can however, have multiple
    /// mutable references to different **types** of `Persist`
    ///
    /// - `Result` covers if the map was able to be borrowed mutably or not.
    /// - Borrowing is checked at runtime.
    ///
    /// # Example
    /// ```
    /// # use tiny_ecs::Entities;
    /// # use tiny_ecs::ECSError;
    /// # fn main() -> Result<(), ECSError> {
    /// # #[derive(Debug, PartialEq)]
    /// # struct Test1 { x: u32 }
    /// # let mut entities = Entities::new(Some(3), Some(3));
    /// # let entity_1 = entities.new_entity()
    /// # .with(Test1 { x: 0 })?.finalise()?;
    /// // Because we later need a ref to the same `Type` of map, the mut ref
    /// // will need to be scoped. If the later ref was of a different type,
    /// // eg: Vector2, then it wouldn't need scoping.
    /// {
    ///     let mut components = entities
    ///         .borrow_mut::<Test1>()?;
    ///     for id in 0..5 {
    ///         if let Some(part) = components.get_mut(id) {
    ///             part.x = 42;
    ///         }
    ///     }
    /// }
    ///
    /// // Now get a ref to the modified part
    /// let components = entities.borrow::<Test1>()?;
    /// let part = components.get(entity_1).unwrap();
    /// assert_eq!(part.x, 42);
    /// # Ok(())
    /// # }
    /// ```
    #[inline(always)]
    pub fn borrow_mut<T: 'static>(&self) -> Result<MapRefMut<T>, ECSError> {
        let type_id = TypeId::of::<T>();
        if let Some((_, type_index)) = self.type_masks.get(&type_id) {
            if let Some(components) = self.components.get(*type_index) {
                return Ok(MapRefMut::new(components)?);
            }
        }
        Err(ECSError::NoComponentMap)
    }

    /// Allows you to borrow mutably without runtime check overhead or to borrow when the standard
    /// borrow rules prevent you from doing so and you enforce safety
    ///
    /// # Safety
    /// Borrows are not checked at compile-time or runtime. An unchecked mutable
    /// on an immutably borrowed `Persist` is UB.
    #[inline(always)]
    pub unsafe fn borrow_mut_unchecked<T: 'static>(&self) -> Result<&mut Persist<T>, ECSError> {
        let type_id = TypeId::of::<T>();
        if let Some((_, type_index)) = self.type_masks.get(&type_id) {
            if let Some(components) = self.components.get(*type_index) {
                let components = (&mut *components.as_ptr())
                    .downcast_mut::<Persist<T>>()
                    .ok_or(ECSError::Downcast)?;
                return Ok(components);
            }
        }
        Err(ECSError::NoComponentMap)
    }
}
